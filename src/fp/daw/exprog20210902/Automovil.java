package fp.daw.exprog20210902;

import java.util.Objects;

public class Automovil {
	/*Defino los atributos de tipo entero como Integer y no int para que puedan ser almacenados en un TreeSet o HashSet
	 * para el tipo uso un enum*/
	 
	private String marca;
	private String modelo;
	private String matricula;
	private Integer año;
	private Tipo tipo;
	private Integer potencia;
	
	
	
	public Automovil(String marca, String modelo, String matricula, Integer año, Tipo tipo, Integer potencia) {
		super();
		this.marca = marca;
		this.modelo = modelo;
		this.matricula = matricula;
		this.año = año;
		this.tipo = tipo;
		this.potencia = potencia;
	}
	/*Solo defino Getters y no Setter, puesto que quiero que sean objetos inmutables*/
	public String getMarca() {
		return marca;
	}
	public String getModelo() {
		return modelo;
	}
	public String getMatricula() {
		return matricula;
	}
	public int getAño() {
		return año;
	}
	public Tipo getTipo() {
		return tipo;
	}
	
	
	@Override
	public String toString() {
		return "Automovil [marca=" + marca + ", modelo=" + modelo + ", matricula=" + matricula + ", año=" + año
				+ ", tipo=" + tipo + ", potencia=" + potencia + "]";
	}
	@Override
	public int hashCode() {
		
		return Objects.hash(marca, modelo, matricula, año, tipo, potencia);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Automovil other = (Automovil) obj;
		if (año == null) {
			if (other.año != null)
				return false;
		} else if (!año.equals(other.año))
			return false;
		if (marca == null) {
			if (other.marca != null)
				return false;
		} else if (!marca.equals(other.marca))
			return false;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		if (modelo == null) {
			if (other.modelo != null)
				return false;
		} else if (!modelo.equals(other.modelo))
			return false;
		if (potencia == null) {
			if (other.potencia != null)
				return false;
		} else if (!potencia.equals(other.potencia))
			return false;
		if (tipo != other.tipo)
			return false;
		return true;
	}
	
	

}
